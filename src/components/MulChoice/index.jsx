import React, { Component } from 'react';
import {FormControl,FormLabel,RadioGroup, FormControlLabel, Radio} from "@material-ui/core";
import {createAction} from "../../store/actions";
import {connect} from "react-redux";
import {actionType} from "../../store/actions/type";

class MulChoice extends Component {
handleRadioChange = (event) => {
    const answers = (this.props.answers);
    let checkExact = answers.find(ans => ans.content === event.target.value);
    this.props.dispatch(
        createAction(actionType.GET_ANSWERS, {
                questionId:this.props.id,
                answer: {
                    content: event.target.value, 
                    exact : checkExact.exact},
                }))
            };
    render() {
        const {id, content, answers}= this.props;
        return (
            <FormControl component="fieldset" fullWidth>
                <FormLabel  color="secondary" component="legend" >{`Câu hỏi 0${id}: ${content}`}</FormLabel>
                <RadioGroup aria-label="gender" name="gender1" onChange={this.handleRadioChange} >
                    {answers.map((item)=>{
                        return <FormControlLabel key={item.id} value={item.content} control={<Radio />} label={item.content} />
                    })}
                </RadioGroup>
            </FormControl>
        );
    }
}

export default connect()(MulChoice);