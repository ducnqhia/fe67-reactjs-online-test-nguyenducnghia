import React, {Component} from 'react';
import {FormLabel, Input, FormControl} from "@material-ui/core";
import {connect} from "react-redux";
import {createAction} from "../../store/actions";
import {actionType} from "../../store/actions/type";

class FillBlank extends Component {
    handechange = (event) =>{
        const answer = (this.props.answers[0].content);
        let checkExact = false;
        if (answer.toLowerCase().trim() === event.target.value.toLowerCase().trim()){
            checkExact = true;
        }
        this.props.dispatch(createAction(actionType.GET_ANSWERS,{
            questionId:this.props.id,
            answer: {
                content: event.target.value, 
                exact: checkExact,
        }}))
    }
    render() {
        const {id, content} = this.props;
        return (
            <FormControl component="fieldset" fullWidth>
                <FormLabel color="secondary" component="legend" >{`Câu hỏi 0${id}: ${content}`}</FormLabel>
                <Input key="1" style={{marginBottom:20}} onChange={this.handechange} color="secondary" placeholder="Trả lời" inputProps={{ 'aria-label': 'description' }} />
            </FormControl>
        );
    }
}

export default connect()(FillBlank);