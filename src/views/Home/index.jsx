import React, { Component } from 'react';
import { Container,Button, Typography} from "@material-ui/core";
import FillInBank from "../../components/FillInBlank";
import MulChoice from "../../components/MulChoice";
import {fetchQuestions} from "../../store/actions/questions"
import { connect } from 'react-redux';

class Home extends Component {
    handleSubmit = (event) =>{
        event.preventDefault();
        let trueAnswer = 0;
        this.props.answers.map((item)=>{
            if(item.answer.exact === true){
                trueAnswer++;
            }
            return trueAnswer;
        });
        if(this.props.answers.length < 8){
            let confirmAction = window.confirm("Bạn còn "+(8 - this.props.answers.length)+" câu hỏi chưa trả lời. Bạn có muốn kết thúc bài thi không?");
            if (confirmAction) {
               return alert("Bạn trả lời đúng "+trueAnswer+"/8 câu hỏi.");
            } 
        }
        alert("Bạn trả lời đúng "+trueAnswer+"/8 câu hỏi.");
    }
    hd = (event) =>{
        event.preventDefault();
        alert("a");
    }
    render() {
        return (  
            <Container maxWidth="md">
                <Typography style={{marginBottom: 20}} variant="h2" color="secondary" align="center" >ONLINE TEST</Typography>
                <form onSubmit={this.handleSubmit}>
                    {this.props.questions.map((item) =>{
                            const {id, content, answers} = item;
                                    if(item.questionType === 1){
                                        return <MulChoice key={id} id={id} content={content} answers={answers} />;
                                    }
                                    return <FillInBank key={id} id={id} content={content} answers={answers}/>;
                                })}
                    <Button type="submit" style={{marginBottom:20}}variant="outlined" color="secondary">
                        Chấm Điểm
                    </Button>
                </form>
            </Container>
        );
    }
    componentDidMount() {
        this.props.dispatch(fetchQuestions);
    }
}
const mapStateToProps = (state) =>({
     questions: state.question.QuestionsList,
     answers: state.question.AnswersList,
});
export default connect(mapStateToProps)(Home);